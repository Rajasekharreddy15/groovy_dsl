def gitUrl = 'https://github.com/Rajasekharreddy15/ops_tree.git'

job('rajshekhar') {
    scm {
        git(gitUrl)
    }
    triggers {

        scm('*/2 * * * *')
    }
   
 steps {
        maven('-e clean test')
    }
 
}
